﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace ej03.Controllers
{
    [ApiController]
    [Route("/api/students")]


    public class StudentController : ControllerBase
    {

        private static IConfiguration _config;

        string[] nombres = { "Camila", "Fernando", "Fabricio", "Jose", "Andrea", "Stephany", "Carlos", "Nicole", "Kiara", "Matilde" };
        string[] apellidos = { "Allende", "Gonzales", "Estenssoro", "Jobs", "Sarmiento", "Duran", "Rosales", "Zambrana", "Andrade", "Burgos" };

        public StudentController(IConfiguration config)
        {
            _config = config;
        }

        [HttpGet]
        public List<Student> GetStudent()
        {

            string projectTitle = _config.GetSection("Project").GetSection("Title").Value;
            string dbConnection = _config.GetConnectionString("Database");
            Console.Out.WriteLine($"We Are connecting to ...{dbConnection}");


            Random r = new Random();
            int est = r.Next(5, 11);
            List<Student> list = new List<Student>();


            for (int i = 0; i < est; i++)
            {
                list.Add(new Student()
                {
                    Name = nombres[r.Next(0, 10)],
                    LastName = apellidos[r.Next(0, 10)]
                });
            }
            return list;
        }

       

        [HttpPost]
        public Student CreateStudent([FromBody] string studentName)//, [FromBody] string studentLastName ) 
        {
            return new Student()
            {
                Name = studentName,
                LastName = "Allende"
            };
        }
        [HttpPut]
        public Student UpdateStudent([FromBody] Student estudiante)
        {
            estudiante.Name = "NuevoNombre";
            estudiante.LastName = "NuevoApellido";
            return estudiante;
        }

        [HttpDelete]
        public Student DeleteStudent([FromBody] Student estudiante)
        {
            estudiante.Name = "Deleted";
            estudiante.LastName = "Deleted";
            return estudiante;
        }
    }
}
